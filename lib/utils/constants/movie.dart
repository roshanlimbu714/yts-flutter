import 'package:movie/models/Movie.model.dart';

List<Movie> SIMILAR_MOVIES = [
  // alt + j/ Ctrl + D
  Movie(
      id: '1',
      name: 'Similar1',
      slug: 'similar1',
      cover:
          'https://cdn.pixabay.com/photo/2017/11/24/10/43/ticket-2974645__480.jpg',
      genre: 'Action',
      description:
          'Something in general Similar. Something in general Similar.Something in general Similar.Something in general Similar.Something in general Similar.Something in general Similar.Something in general Similar.Something in general Similar.Something in general Similar.',
      rating:3,
      runtime: '2hr 1min',
      year: '2021'),
  Movie(
      id: '2',
      name: 'Similar2',
      slug: 'similar2',
      cover:
          'https://cdn.pixabay.com/photo/2019/10/17/21/17/joker-4557864__340.jpg',
      genre: 'Action',
      description:
          'Something in general Similar. Something in general Similar.Something in general Similar.Something in general Similar.Something in general Similar.Something in general Similar.Something in general Similar.Something in general Similar.Something in general Similar.',
      rating: 1,
      runtime: '2hr 2min',
      year: '2021'),
  Movie(
      id: '3',
      name: 'Similar3',
      slug: 'similar3',
      cover:
          'https://cdn.pixabay.com/photo/2019/04/17/16/12/man-4134645__340.jpg',
      genre: 'Action',
      description:
          'Something in general Similar. Something in general Similar.Something in general Similar.Something in general Similar.Something in general Similar.Something in general Similar.Something in general Similar.Something in general Similar.Something in general Similar.',
      rating: 4,
      runtime: '2hr 3min',
      year: '2021'),
  Movie(
      id: '4',
      name: 'Similar4',
      slug: 'similar4',
      cover:
          'https://cdn.pixabay.com/photo/2016/08/30/17/08/hare-1631435__340.jpg',
      genre: 'Action',
      description:
          'Something in general Similar. Something in general Similar.Something in general Similar.Something in general Similar.Something in general Similar.Something in general Similar.Something in general Similar.Something in general Similar.Something in general Similar.',
      rating: 2,
      runtime: '2hr 4min',
      year: '2021'),
];

List<Movie> FEATURED_MOVIES = [
  Movie(
      id: '1',
      name: 'Featured1',
      slug: 'featured1',
      cover:
          'https://cdn.pixabay.com/photo/2017/11/24/10/43/ticket-2974645__480.jpg',
      genre: 'Action',
      description:
          'Something in general Feature. Something in general Feature.Something in general Feature.Something in general Feature.Something in general Feature.Something in general Feature.Something in general Feature.Something in general Feature.Something in general Feature.',
      rating: 3,
      runtime: '2hr 1min',
      year: '2021'),
  Movie(
      id: '2',
      name: 'Featured2',
      slug: 'featured2',
      cover:
          'https://cdn.pixabay.com/photo/2019/10/17/21/17/joker-4557864__340.jpg',
      genre: 'Action',
      description:
          'Something in general Feature. Something in general Feature.Something in general Feature.Something in general Feature.Something in general Feature.Something in general Feature.Something in general Feature.Something in general Feature.Something in general Feature.',
      rating: 4,
      runtime: '2hr 2min',
      year: '2021'),
  Movie(
      id: '3',
      name: 'Featured3',
      slug: 'featured3',
      cover:
          'https://cdn.pixabay.com/photo/2019/04/17/16/12/man-4134645__340.jpg',
      genre: 'Action',
      description:
          'Something in general Feature. Something in general Feature.Something in general Feature.Something in general Feature.Something in general Feature.Something in general Feature.Something in general Feature.Something in general Feature.Something in general Feature.',
      rating: 4,
      runtime: '2hr 3min',
      year: '2021'),
  Movie(
      id: '4',
      name: 'Featured4',
      slug: 'featured4',
      cover:
          'https://cdn.pixabay.com/photo/2016/08/30/17/08/hare-1631435__340.jpg',
      genre: 'Action',
      description:
          'Something in general Feature. Something in general Feature.Something in general Feature.Something in general Feature.Something in general Feature.Something in general Feature.Something in general Feature.Something in general Feature.Something in general Feature.',
      rating: 2,
      runtime: '2hr 4min',
      year: '2021'),
];

List<Movie> POPULAR_MOVIES = [
  Movie(
      id: '1',
      name: 'Popular1',
      slug: 'popular1',
      cover:
          'https://cdn.pixabay.com/photo/2017/02/18/18/09/skull-and-crossbones-2077840__340.jpg',
      genre: 'Action',
      description:
          'Something in general Popular. Something in general Popular.Something in general Popular.Something in general Popular.Something in general Popular.Something in general Popular.Something in general Popular.Something in general Popular.Something in general Popular.',
      rating: 3,
      runtime: '2hr 1min',
      year: '2021'),
  Movie(
      id: '2',
      name: 'Popular2',
      slug: 'popular2',
      cover:
          'https://cdn.pixabay.com/photo/2016/01/13/22/54/man-1139066__340.jpg',
      genre: 'Action',
      description:
          'Something in general Popular. Something in general Popular.Something in general Popular.Something in general Popular.Something in general Popular.Something in general Popular.Something in general Popular.Something in general Popular.Something in general Popular.',
      rating: 4,
      runtime: '2hr 2min',
      year: '2021'),
  Movie(
      id: '3',
      name: 'Popular3',
      slug: 'popular3',
      cover:
          'https://cdn.pixabay.com/photo/2020/04/20/18/10/cinema-5069314__340.jpg',
      genre: 'Action',
      description:
          'Something in general Popular. Something in general Popular.Something in general Popular.Something in general Popular.Something in general Popular.Something in general Popular.Something in general Popular.Something in general Popular.Something in general Popular.',
      rating: 1,
      runtime: '2hr 3min',
      year: '2021'),
  Movie(
      id: '4',
      name: 'Popular4',
      slug: 'popular4',
      cover:
          'https://cdn.pixabay.com/photo/2016/08/30/17/08/hare-1631435__340.jpg',
      genre: 'Action',
      description:
          'Something in general Popular. Something in general Popular.Something in general Popular.Something in general Popular.Something in general Popular.Something in general Popular.Something in general Popular.Something in general Popular.Something in general Popular.',
      rating: 4,
      runtime: '2hr 4min',
      year: '2021'),
];

List<Movie> LATEST_ARRIVALS = [
  Movie(
      id: '1',
      name: 'Latest1',
      slug: 'latest1',
      cover:
          'https://cdn.pixabay.com/photo/2015/05/15/09/13/demonstration-767982__340.jpg',
      genre: 'Action',
      description:
          'Something in general Latest1. Something in general Latest1.Something in general Latest1.Something in general Latest1.Something in general Latest1.Something in general Latest1.Something in general Latest1.Something in general Latest1.Something in general Latest1.',
      rating: 3,
      runtime: '2hr 1min',
      year: '2021'),
  Movie(
      id: '2',
      name: 'Latest2',
      slug: 'latest2',
      cover:
          'https://cdn.pixabay.com/photo/2017/01/25/17/35/camera-2008489__340.png',
      genre: 'Action',
      description:
          'Something in general Latest2. Something in general Latest2.Something in general Latest2.Something in general Latest2.Something in general Latest2.Something in general Latest2.Something in general Latest2.Something in general Latest2.Something in general Latest2.',
      rating:2,
      runtime: '2hr 2min',
      year: '2021'),
  Movie(
      id: '3',
      name: 'Latest3',
      slug: 'latest3',
      cover:
          'https://cdn.pixabay.com/photo/2017/09/04/09/37/cinema-strip-2713352__340.jpg',
      genre: 'Action',
      description:
          'Something in general Latest3. Something in general Latest3.Something in general Latest3.Something in general Latest3.Something in general Latest3.Something in general Latest3.Something in general Latest3.Something in general Latest3.Something in general Latest3.',
      rating: 1,
      runtime: '2hr 3min',
      year: '2021'),
  Movie(
      id: '4',
      name: 'Latest4',
      slug: 'latest4',
      cover:
          'https://cdn.pixabay.com/photo/2016/04/14/13/06/landscape-1328858__340.jpg',
      genre: 'Action',
      description:
          'Something in general Latest4. Something in general Latest4.Something in general Latest4.Something in general Latest4.Something in general Latest4.Something in general Latest4.Something in general Latest4.Something in general Latest4.Something in general Latest4.',
      rating: 2,
      runtime: '2hr 4min',
      year: '2021'),
];
