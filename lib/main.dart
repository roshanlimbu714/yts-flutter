import 'package:flutter/material.dart';
import 'package:movie/screens/Popular.screen.dart';
import 'package:movie/screens/Search.screen.dart';
import './screens/Landing.screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        fontFamily: 'Poppins',
        primarySwatch: Colors.deepPurple,
      ),
      home: LandingScreen(),
      routes:{
        '': (ctx) => LandingScreen(),
        '/search': (ctx) => Search(),
        '/popular': (ctx) => Popular(),
      }
    );
  }
}


// div => Container, div with a shadow => Card, Padding, Margin,
// button => TextButton, elevatedbutton