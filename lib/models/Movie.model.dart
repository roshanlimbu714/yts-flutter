import 'package:flutter/material.dart';

class Movie {
  final String id;
  final String name;
  final String slug;
  final String cover;
  final String genre;
  final String runtime;
  final String year;
  final int rating;
  final String description;

  Movie({
    @required this.id,
    @required this.name,
    @required this.slug,
    @required this.cover,
    @required this.genre,
    @required this.runtime,
    @required this.year,
    @required this.rating,
    @required this.description,
  });
}