import 'package:flutter/material.dart';
import 'package:movie/utils/constants/styles.dart';
import 'package:movie/widgets/Container/SimilarMovies.partials.dart';

class MovieDetail extends StatelessWidget {
  final movie;

  MovieDetail(this.movie);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(movie.name)),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 16),
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                margin:EdgeInsets.only(top:16),
                width: double.infinity,
                height: 200,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    image: DecorationImage(
                        image: NetworkImage(movie.cover), fit: BoxFit.cover)),
              ),
              Container(
                margin: EdgeInsets.only(top: 16),
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      movie.name,
                      style: kContainerTitle,
                    ),
                    Row(
                      children: [
                        Chip(
                          label: Text(movie.year),
                          backgroundColor: Colors.blueAccent,
                        ),
                        Chip(
                          label: Text(movie.genre),
                          backgroundColor: Colors.deepPurple,
                          labelStyle: TextStyle(color: Colors.white),
                        ),
                        Chip(label: Text(movie.runtime)),
                      ],
                    ),
                    Row(
                      children: [
                        for (var i = 0; i < movie.rating; i++)
                          Icon(
                            Icons.star,
                            color: Colors.orange,
                          )
                      ],
                    )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 16),
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Plot summary',
                      style: kContainerTitle,
                    ),
                    Text(movie.description)
                  ],
                ),
              ),
              SimilarMovies()
            ],
          ),
        ),
      ),
    );
  }
}
