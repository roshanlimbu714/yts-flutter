import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movie/utils/constants/styles.dart';
import 'package:movie/widgets/Container/FeaturedMovies.partials.dart';
import 'package:movie/widgets/Container/LatestArrivals.container.dart';
import 'package:movie/widgets/Container/MovieSlider.container.dart';
import 'package:movie/widgets/Container/PopularMovies.container.dart';
import 'package:movie/widgets/partials/BottomNav.partials.dart';

class LandingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('YTS')),
      body: Container(
        padding: EdgeInsets.only(top: 16, left: 16, right: 16),
        height: MediaQuery.of(context).size.height - 56,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(bottom: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Hello John',
                        style: TextStyle(
                            fontSize: 36, fontWeight: FontWeight.w300, color: Colors.deepPurple)),
                    Text(
                      'What are we watching today?',
                      style:
                        kContainerTitle,
                    )
                  ],
                ),
              ),
              MovieSlider(),
              FeaturedMovies(),
              LatestArrivals(),
              PopularMovies()
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomNav(),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () {},
      //   child: Icon(Icons.nat),
      //   backgroundColor: Colors.orange[800],
      // ),
    );
  }
}
