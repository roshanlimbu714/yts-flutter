import 'package:flutter/material.dart';
import 'package:movie/screens/MovieDetail.screen.dart';

class MovieCard extends StatelessWidget {
  final movie;

  MovieCard(this.movie);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: ()=>Navigator.of(context).push(MaterialPageRoute(
          builder: (ctx) => MovieDetail(movie)
      )),
      child: Container(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(right: 8, bottom:8),
              height: 200,
              width: 150,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  image: DecorationImage(image: NetworkImage(movie.cover), fit: BoxFit.cover)
              ),
            ),
            Text(movie.name,style: TextStyle(
              fontSize: 16,
            ),)
          ],
        ),
      ),
    );
  }
}
