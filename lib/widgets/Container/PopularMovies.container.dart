import 'package:flutter/material.dart';
import 'package:movie/models/Movie.model.dart';
import 'package:movie/utils/constants/styles.dart';
import 'package:movie/widgets/Common/MovieCard.common.dart';

class PopularMovies extends StatelessWidget {
  final List<Movie> movies = [

  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 32),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Popular Movies',
            style:  kContainerTitle,
          ),
          Container(
            margin: EdgeInsets.only(top:8),
            width: double.infinity,
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  ...movies.map((movie) =>
                      MovieCard(movie)
                  ).toList()
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
