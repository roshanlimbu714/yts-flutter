import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:movie/screens/MovieDetail.screen.dart';
import 'package:movie/utils/constants/movie.dart';

class MovieSlider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CarouselSlider(
        options: CarouselOptions(height: 400.0),
        items: FEATURED_MOVIES.map((i) {
          return Builder(
            builder: (BuildContext context) {
              return GestureDetector(
                  onTap: () => Navigator.of(context).push(
                      MaterialPageRoute(builder: (ctx) => MovieDetail(i))),
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 4),
                    width: double.infinity,
                    height: 200,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      image: DecorationImage(
                          image: NetworkImage(i.cover), fit: BoxFit.cover),
                    ),
                  ));
            },
          );
        }).toList());
  }
}
