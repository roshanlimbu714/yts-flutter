import 'package:flutter/material.dart';
import 'package:movie/models/Movie.model.dart';
import 'package:movie/utils/constants/movie.dart';
import 'package:movie/utils/constants/styles.dart';
import 'package:movie/widgets/Common/MovieCard.common.dart';

class LatestArrivals extends StatelessWidget {
  final List<Movie> movies = LATEST_ARRIVALS;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 32),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Latest Movies',
            style: kContainerTitle,
          ),
          Container(
            margin: EdgeInsets.only(top:8),
            width: double.infinity,
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  ...movies.map((movie) =>
                      MovieCard(movie)
                  ).toList()
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
