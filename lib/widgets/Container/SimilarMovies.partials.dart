import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movie/models/Movie.model.dart';
import 'package:movie/utils/constants/movie.dart';
import 'package:movie/utils/constants/styles.dart';
import '../Common/MovieCard.common.dart';

class SimilarMovies extends StatelessWidget {
  final List<Movie> movies = SIMILAR_MOVIES;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 32),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Similar Movies',
            style: kContainerTitle,
          ),
          Container(
            margin: EdgeInsets.only(top: 8),
            width: double.infinity,
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  ...movies.map((movie) =>
                   MovieCard(movie)
                  ).toList()
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
